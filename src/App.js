import { useState } from "react";
import { Card } from "antd";
import "./App.css";
import "antd/dist/antd.css";
import { Button } from "antd";

function App() {
  const { Meta } = Card;
  const [show, setShow] = useState(false);
  const [user, setUser] = useState("");

  const handleToggle = () => {
    fetch("https://api.github.com/users/adilson")
      .then((res) => res.json())
      .then((dados) => {
        setUser(dados);
        console.log(dados);
      });

    setShow(!show);
  };
  return (
    <div className="App">
      <header className="App-header">
        <Button shape="round" type="primary" onClick={handleToggle}>
          Click-me
        </Button>
        {show && (
          <Card>
            <img src={user.avatar_url} />
            <h4>{user.name}</h4>
            <h4>{user.location}</h4>
            <h4>{user.email}</h4>
          </Card>
        )}
      </header>
    </div>
  );
}

export default App;
